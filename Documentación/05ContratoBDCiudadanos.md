![Help Builder Web Site](./Logo.png)
# Proyecto_SA_Grupo2
### Modulo "Registro de usuarios en iVoting"

**ID: SA-005**
<br>
**Nombre: BD Ciudadanos**


<table>
<thead>
	<tr>
		<th>Historia de usuario</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Como administrador del sistema
Quiero un servicio REST
Para poder consultar la información de los ciudadanos.
</td>
	</tr>
</tbody>
</table>




<table>
<thead>
	<tr>
		<th>Prioridad</th>
		<th>Estimado</th>
		<th>Modulo</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Alta</td>
		<td>10 puntos</td>
		<td>Registro</td>
	</tr>
</tbody>
</table>



**Criterios de Aceptación**
<br>

El servicio debe tener la siguiente configuración:



<br>
<table>
<tbody>
	<tr>
	<td>Ruta</td>
	<td>/api/ciudadanos</td>
	</tr>
	<tr>
	<td>Método</td>
	<td>GET</td>
	</tr>
</tbody>
</table>
<table>
<tbody>
	<tr>
	<td>Formato de Salida</td>
	<td>JSON</td>
	</tr>
	<tr>
	<td>Código de respuesta exitosa</td>
	<td>HTTP 200</td>
	</tr>
</tbody>
</table>

<br>

**Header**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Content-type</td>
		<td>header</td>
		<td>application/json</td>
	</tr>
	<tr>
		<td>Authentication</td>
		<td>header</td>
		<td>token <TOKEN></td>
	</tr>
</tbody>
</table>

<br>


**Parametros de Entrada**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>cui</td> 
		<td>Entero</td>
		<td>
Código único de identificación
</td>
	</tr>
</tbody>
</table>
<br>

**Parametros de Salida Exitosa**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>status</td> 
		<td>Entero</td>
		<td>
Indica el estado actual del ciudadano en el sistema.
</td>
	</tr>
	<tr>
		<td>cui</td> 
		<td>Cadena</td>
		<td>
Código único de identificación de una persona.
</td>
	</tr>
	<tr>
		<td>nombres</td> 
		<td>Cadena</td>
		<td>
Nombre completo del usuario
</td>
	</tr>
	<tr>
		<td>Apellidos</td> 
		<td>Cadena</td>
		<td>
Apellidos completos del usuario
</td>
	</tr>
	<tr>
		<td>fecha_nacimiento</td> 
		<td>Cadena</td>
		<td>Formato según ISO 8601 YYYY-MM-DD
		</td>
	</tr>
	<tr>
		<td>lugar_municipio</td> 
		<td>Cadena</td>
		<td>Nombre del municipio: ‘Mixco’, ‘Chiantla’, ‘Villa Nueva</td>
	</tr>
	<tr>
		<td>lugar_departamento</td> 
		<td>Cadena</td>
		<td>Nombre de departamento:
‘Petén’, ‘Guatemala’
		</td>
	</tr>
	<tr>
		<td>lugar_pais</td> 
		<td>Cadena</td>
		<td>Nombre de país: ‘Guatemala’, ‘El Salvador’
		</td>
	</tr>
	<tr>
		<td>nacionalidad</td> 
		<td>Cadena</td>
		<td>Nacionalidad del ciudadano:
‘Guatemala’, ‘El Salvador’
		</td>
	</tr>
	<tr>
		<td>sexo</td> 
		<td>Cadena</td>
		<td>
Dos posibles valores válidos:
masculino -> ‘M‘ 
femenino   -> ‘F’
		</td>
	</tr>
	<tr>
		<td>Estado Civil</td> 
		<td>Cadena</td>
		<td>Valores:
0 soltero
1 casado
		</td>
	</tr>
	<tr>
		<td>servicio_militar</td> 
		<td>Cadena</td>
		<td>Basados en el art. 15 de LEPP los valores son:
0 No presta servicio
1 Presta servicio 
		</td>
	</tr>
	<tr>
		<td>privado_libertad</td> 
		<td>Cadena</td>
		<td>0 libre, 1 privado
		</td>
	</tr>
	<tr>
		<td>foto</td> 
		<td>Cadena</td>
		<td>En base64
		</td>
	</tr>
</tbody>
</table>
<br>

**Código de respuesta fallida**
<table>
<thead>
	<tr>
		<th>Código</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>403</td> 
		<td>Fallo al obtener la información de los ciudadanos.</td>
	</tr>

</tbody>
</table>
<br>

**Parámetros de salida fallida:**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>status</td> 
		<td>Cadena</td> 
		<td>
Indica el tipo de error ocurrido.
</td> 
	</tr>
	<tr>
		<td>mensaje</td> 
		<td>Cadena</td> 
		<td>Muestra un detalle del error ocurrido.</td> 
	</tr>
</tbody>
</table>

<br>


**Ejemplo de parámetros de entrada:**
>{
	"cui":
>}

**Ejemplo de parámetros de salida exitosa:**
>Ej 1:{
	"status": 200,
            "cui": "",
            "nombres": "",
            "apellidos": "",
            "fecha_nacimiento": "",
            "lugar_municipio": "",
            "lugar_departamento": "",
            "lugar_pais": "",
             "nacionalidad": "",
             "sexo": "",
             "estado_civil": "",
             "servicio_militar": "",
             "privado_libertad": "",
              "foto": ""
}, Ej 2:{
	"status": 200,
            "cui":"1111222223333",
            "nombres":"Sandra",
            "apellidos":"Solis",
            "fecha_nacimiento":"1994-11-01",
            "lugar_municipio": "Mixco",
            "lugar_departamento":"Guatemala",
            "lugar_pais":"Guatemala",
             "nacionalidad":"Guatemala",
             "sexo": "F",
             "estado_civil": 0,
             "servicio_militar": 0,
             "privado_libertad": 0,
              "foto":"aG9sYS5qcGc="
> }



**Ejemplo de parámetros de salida fallida:**
>{
	"status": 400,
	"mensaje" :    
>}
