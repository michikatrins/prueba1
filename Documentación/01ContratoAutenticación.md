![Help Builder Web Site](./Logo.png)
# Proyecto_SA_Grupo2
### Modulo "Registro de usuarios en iVoting"

**ID: SA-001**
<br>
**Nombre: Autenticacion usuarios**


<table>
<thead>
	<tr>
		<th>Historia de usuario</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Como administrador del sistema
Quiero un servicio REST
Para poder autenticar a los usuarios en el sistema
</td>
	</tr>
</tbody>
</table>




<table>
<thead>
	<tr>
		<th>Prioridad</th>
		<th>Estimado</th>
		<th>Modulo</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Alta</td>
		<td>10 puntos</td>
		<td>Sesión</td>
	</tr>
</tbody>
</table>



**Criterios de Aceptación**
<br>
El servicio se debe conectar a un servidor JWT y retornar un token válido para autenticar al usuario y retornar los datos del usuario.

El servicio debe tener la siguiente configuración:

<br>
<table>
<tbody>
	<tr>
	<td>Ruta</td>
	<td>/api/autenticacion</td>
	</tr>
	<tr>
	<td>Método</td>
	<td>POST</td>
	</tr>
	<tr>
	<td>Formato de entrada</td>
	<td>JSON</td>
	</tr>
</tbody>
</table>

<br>

**Header**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Content-type</td>
		<td>header</td>
		<td>application/json</td>
	</tr>
	<tr>
		<td>Authentication</td>
		<td>header</td>
		<td>token <TOKEN></td>
	</tr>
</tbody>
</table>

<br>

**Parametros de Entrada**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>pin</td> 
		<td>Entero</td>
		<td> Código de 6 dígitos del usuario.</td>
	</tr>
	<tr>
	<td>pass</td>
	<td>Entero</td>
	<td>Codigo de 6 digitos de usuario</td>
	</tr>
	<tr>
		<td>rol</td> 
		<td>Entero</td>
		<td>
1- Administrador 
2- Ciudadano
</td>
	</tr>
</tbody>
</table>
<br>



<table>
<tbody>
	<tr>
	<td>Formato de Salida</td>
	<td>JSON</td>
	</tr>
	<tr>
	<td>Código de respuesta exitosa</td>
	<td>HTTP 200</td>
	</tr>
</tbody>
</table>
<br>

**Parametros de Salida Exitosa**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>token</td> 
		<td>Cadena</td>
		<td>
Token de autenticación.
</td>
</tbody>
</table>
<br>

**Código de respuesta fallida**
<table>
<thead>
	<tr>
		<th>Código</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>403</td> 
		<td>Fallo en la autenticación. </td>
	</tr>

</tbody>
</table>
<br>

**Parámetros de salida fallida:**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>error</td> 
		<td>Cadena</td> 
		<td>
Indica el tipo de error ocurrido.
</td> 
	</tr>
	<tr>
		<td>mensaje</td> 
		<td>Cadena</td> 
		<td>Muestra un detalle del error ocurrido.</td> 
	</tr>
</tbody>
</table>
<br>


**Ejemplo de parámetros de entrada:**
<br>
>{
	"email":,
	"pass":
>}

**Ejemplo de parámetros de salida exitosa:**
<br>
>{
	"status": 200,
	"token" :    
>}

**Ejemplo de parámetros de salida fallida:**
<br>
>{
	"status": 400,
	"mensaje" “”:    
>}
