import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salir',
  templateUrl: './salir.component.html',
  styleUrls: ['./salir.component.css']
})
export class SalirComponent implements OnInit {
  userid: any;
  constructor(private router: Router) { }

  ngOnInit(): void {
    sessionStorage.removeItem("userid");
    this.router.navigate(['registro']);
    /*this.userid = sessionStorage.getItem('userid');
    if (!this.userid){
      this.router.navigate(['login']);
    }
    sessionStorage.clear();
    this.router.navigate(['inicio']);*/
  }
}