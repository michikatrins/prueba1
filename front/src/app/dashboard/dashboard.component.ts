import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  userid: string = "";
  resultado: any = [];
  elecciones: any = [];
  alerta: string = '';
  color:string = "#65ff00";
  conex: any;

  miArray:any[] = [{
    "titulo" : "Elecciones presidenciales",
    "descripcion": "Elecciones para presidente de la República de Guatemala",
    "fecha_inicio": "DD/MM/YYYY",
    "hora_inicio" : "13:00",
    "fecha_fin": "DD/MM/YYYY",
    "hora_fin" : "16:00",
    "opciones": [ 1 , 2 ,5, 6]          
    }];

    //this.miArray = JSON.parse(tuRespuesta);
    
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    //this.userid = sessionStorage.getItem('userid');
    /*if (!this.userid){
      this.router.navigate(['login']);
    }*/
    this.mostrar();
  }
  
  mostrar(): void {
    
  }

}
