import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IpServiceService } from '../ip-service.service';
import { ConectionService } from '../services/conection.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})

export class RegistroComponent implements OnInit {

  constructor(private ip:IpServiceService, private con: ConectionService, private router: Router) {
  }

  Cui: string = '';
  Name: string = '';
  Apellido: string = '';
  Email: string = '';
  Fecha: string = '';
  Municipio: string = '';
  Departamento: string = '';
  Sexo: string = '';
  Pais: string = '';
  Civil: string = '';
  Militar: string = '';
  Privado: string = '';
  Padron: string = '';
  Foto: string = '';
  password: string = '';
  alerta: string = '';
  title = 'DemoApp';  
  ipAddress:string | undefined;  

  ngOnInit()  
  {  
    this.getIP();  
  }  
  
  getIP()  
  {  
    this.ip.getIPAddress().subscribe((res:any)=>{  
      this.ipAddress=res.ip;  
    });  
  }  

  createRegistroObject(cui: string, nombre:string, apellidos: string, email: string, fecha_nacimiento: string, 
    municipio: string, departamento: string, pais: string, sexo: string, est_civil: string, 
    est_militar: string, libertad: any, padro: any, fotografia: string, ip: any) {
    return { cui: cui, nombre: nombre, apellidos: apellidos, email: email, fecha_nacimiento: fecha_nacimiento, municipio: municipio, departamento: departamento, pais: pais, sexo: sexo, est_civil: est_civil, est_militar: est_militar, libertad: libertad, padro: padro, fotografia: fotografia, ip: ip };
  }


  onSubmit() {
  }

  registro() {
    let registros = this.createRegistroObject(this.Cui, this.Name, this.Apellido, this.Email, this.Fecha,
        this.Municipio, this.Departamento, this.Pais, this.Sexo, this.Civil,
        this.Militar, this.Privado, this.Padron, this.Foto, this.ipAddress);
    this.con.PostRequest('login', registros).toPromise()
    .then((res) => {
      sessionStorage.setItem('userid', res.userid);
      console.log("ya funciono");  
      console.log(res);
      this.router.navigate(['inicioVotante']);
    })
    .catch((err)=>{
      setTimeout(() => this.alerta = err.error.mensaje, 0);
    });

    //this.router.navigate(['inicioVotante']);
  }

  enviarCorreo(){
    /*
    this.http.sendEmail("http://localhost:3000/sendmail", user).subscribe(
      data => {
        let res:any = data; 
        console.log(
          `👏 > 👏 > 👏 > 👏 ${user.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      err => {
        console.log(err);
        this.loading = false;
        this.buttionText = "Submit";
      },() => {
        this.loading = false;
        this.buttionText = "Submit";
      }
    );
    */
  }
}


