import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConectionService } from '../services/conection.service';
import { ApiService } from '../services/api.service';
import { IpServiceService } from '../ip-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  correo: string = '';
  password: string = '';
  alerta: string = '';
  userid: any;
  ipAddress:string | undefined; 
  getIP()  
  {  
    this.ip.getIPAddress().subscribe((res:any)=>{  
      this.ipAddress=res.ip;  
    });  
  }  


  constructor(private ip:IpServiceService, private con: ConectionService, private router: Router) { }
  
  ngOnInit(): void {
    sessionStorage.clear();
    this.userid = sessionStorage.getItem('userid');
    console.log(this.userid)
    if (!this.userid || this.userid  == null){
      this.router.navigate(['login']);
    }
    this.getIP();
  }

  createLoginObject(correo: string, password: string, ip: any) {
    return { correo: correo, password: password ,ip: ip };
  }

  login() {
    let login = this.createLoginObject(this.correo, this.password, this.ipAddress);
    this.con.PostRequest('login', login).toPromise()
      .then((res) => {
        sessionStorage.setItem('userid', res.userid);
        console.log("ya funciono");  
        console.log(res);
        this.router.navigate(['inicioVotante']);
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  
}
