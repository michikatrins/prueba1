import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class ResultadosService {

  endpoint = 'http://localhost:3005/api/';

  httpOptions = {
    headers : new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }


  getResultados(ideleccion: Number) {
    return this.http.get<any>(this.endpoint+ideleccion, this.httpOptions);
  }

}
