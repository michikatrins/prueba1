import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResVotacionComponent } from './res-votacion.component';

describe('ResVotacionComponent', () => {
  let component: ResVotacionComponent;
  let fixture: ComponentFixture<ResVotacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResVotacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResVotacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
