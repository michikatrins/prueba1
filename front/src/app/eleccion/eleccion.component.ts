import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';
import { IpServiceService } from '../ip-service.service';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-eleccion',
  templateUrl: './eleccion.component.html',
  styleUrls: ['./eleccion.component.css']
})


export class EleccionComponent implements OnInit {

  userid: string = "";
  titulo: any;
  descripcion: any;
  fecha_inicio: any;
  hora_inicio: any;
  fecha_fin: any;
  hora_fin: any;
  opciones: any;
  alerta: string = "";

  /*
  {
    "titulo": "Elecciones presidenciales",
    "descripcion": "Elecciones para presidente",
      "fecha_inicio": "DD/MM/YYYY",
      "hora_inicio" : "13:00",
    "fecha_fin": "DD/MM/YYYY",
      "hora_fin" : "16:00",
      "opciones": [ 1 , 2 ,5, 6]            
  }
  */

  //http://34.70.175.232:81/api/listaelecciones

  constructor(private ip:IpServiceService, private con: ConectionService, private router: Router) { }
  
  createEleccionObject(titulo: string, descripcion: string, fecha_inicio: string, hora_inicio: string, fecha_fin: string, hora_fin: string, opciones: any) {
    return { titulo, descripcion, fecha_inicio, hora_inicio, fecha_fin, hora_fin, opciones };
  }

  ngOnInit(): void {
    //this.userid = sessionStorage.getItem('userid');
    /*if (!this.userid){
      this.router.navigate(['login']);
    }*/
  }
  
  eleccion(): void {
    let elec = this.createEleccionObject(this.titulo, this.descripcion, this.fecha_inicio, this.hora_inicio, this.fecha_fin, this.hora_fin, this.opciones );
    this.con.PostRequest('api/listaelecciones', elec).toPromise()
      .then((res) => {
        sessionStorage.setItem('userid', res.userid);
        console.log("se agrego");  
        console.log(res);
        //this.router.navigate(['inicioVotante']);
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

}
