import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio-votante',
  templateUrl: './inicio-votante.component.html',
  styleUrls: ['./inicio-votante.component.css']
})
export class InicioVotanteComponent implements OnInit {

  userid: string = "";
  resultado: any = [];
  elecciones: any = [];
  alerta: string = '';
  color:string = "#65ff00";
  conex: any;
    
  constructor(private con: ConectionService, private router: Router) { }

  ngOnInit(): void {
    //this.userid = sessionStorage.getItem('userid');
    //if (!this.userid){
      //this.router.navigate(['login']);
    //}
    this.mostrar();
  }
  
  mostrar(): void {
    
  }

}
