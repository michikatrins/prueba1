import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioVotanteComponent } from './inicio-votante.component';

describe('InicioVotanteComponent', () => {
  let component: InicioVotanteComponent;
  let fixture: ComponentFixture<InicioVotanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InicioVotanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioVotanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
