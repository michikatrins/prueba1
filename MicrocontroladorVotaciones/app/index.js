const express = require('express');
const app = express();
const mysql = require('mysql');
const morgan = require('morgan');
const myConnection = require('express-myconnection');
const bodyParser = require('body-parser');

var jwt=require('jsonwebtoken');
const fs = require('fs');
const path = require('path');

require('./database');

let respuesta = {
    error: false,
    codigo: 200
   };


//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./src/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

// middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
    host: 'db-mysql',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'sa_bd'
  }, 'single'));
app.use(express.urlencoded({extended: true}));

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.listen(3000, ()=>{
    console.log('server on port 3000');
})

var logger = fs.createWriteStream('./src/log.txt', {
    flags: 'a'
});

function addLog(newlog){
    logger.write(newlog+"\n");
}

// Authorization: Bearer <token>
function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];
        req.jwt=bearerToken;
        next();
    }else{
        res.sendStatus(403) //ruta o acceso prohibido
    }
}

function validateScope(scopes,idruta)
{
    var result=false;
    scopes.forEach(function(word) {
        console.log(word);
        console.log(idruta);
        if (word === idruta) {
          console.log(true);
          result=true;
        }

      });
      return result;
}

//agregar una nueva votacion.
app.post('/votacion', verifytoken, function (req, res) {
    jwt.verify(req.jwt,publicKey,verifyOptions,(err)=>{
        if(err){   
            addLog(" >> Error en token.");   
            respuesta.error=true
            respuesta.codigo=403
            res.send(respuesta);
        }else{
            var decoded = jwt.decode(req.jwt, {complete: true});

            var exist=validateScope(decoded.payload.scopes,'usuarios.usuario.post');
            //console.log(exist);
            if(exist){
                req.getConnection((err, conn) => {
                    conn.query('INSERT INTO  votacion set ?', req.body, (err, rows) =>{
                        if(err){
                            console.log(err);
                            res.json(err);
                        }
                        addLog(" >> se creo un voto ", req.body.nombres);
                        res.json({
                            header: decoded.header,
                            payload: decoded.payload,
                            mensaje: "voto agregado satisfactoriamente",                        
                        });
                    })
                });
            }else{
                addLog(" >> Error en token crear usuario.");
                res.sendStatus(403);
            }
        }
    });
});

//devolver votos por id
app.get('/voto/:id', verifytoken, function (req, res) {
    jwt.verify(req.jwt,publicKey,verifyOptions,(err)=>{
        if(err){  
            addLog(" >> Error en token devolver un solo voto.");    
            respuesta.error=true
            respuesta.codigo=403
            res.send(respuesta);
        }else{
            var decoded = jwt.decode(req.jwt, {complete: true});

            var exist=validateScope(decoded.payload.scopes,'usuarios.usuario.get');
            console.log(exist);
            if(exist){
                const  id  = req.params.id;
                console.log(id);
                req.getConnection((err, conn) => {
                    conn.query("SELECT * FROM votacion WHERE id = ?", [id], (err, rows) => {
                        if(err){
                            console.log(err);
                            res.json(err);
                        }
                        addLog(" >> voto devuelto.");
                        res.json({
                            header: decoded.header,
                            payload: decoded.payload,
                            mensaje: "voto devuelto.",
                            rows: rows[0],
                        });
                    });
                });
            }else{
                addLog(" >> Error en token.");
                res.sendStatus(403);
            }
        }
    });
});
