var axios = require('axios');
const mysql = require('mysql');

var fs = require('fs');

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

const con = mysql.createConnection({

  
    host: 'db-mysql-aut',
    port: '3306',
    user: 'admin',
    password: 'root',

    database: 'prsa'
});
let proyectobd = {};

proyectobd.all = () => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM eleccion;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });


};
proyectobd.one = (ideleccion) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "SELECT * eleccion WHERE ideleccion =" + ideleccion + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res[0]);
        });
    });


};
proyectobd.insertarEleccion = (titulo,descripcion,fechai, horai,fechaf,horaf,opciones) => {
    
        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO eleccion(titulo,descripcion,fecha_inicio,hora_inicio,fecha_fin,hora_fin,opciones) VALUES ('" + titulo + "','" + descripcion + "','" + fechai + "','"+ horai +"','" + fechaf + "','"+ horaf +"','"+ opciones+"');";
        con.query(query, (err, res) => {
        if (err) throw err;           

        addLog("eleccion creada: "+res.insertId+"\n");
        
        resolve(res.insertId);
        });
    });


};

module.exports = proyectobd;
