import { AxiosError, AxiosResponse} from "axios";
import { json } from "express";
var express = require('express');
var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
const path = require('path');
var fs = require('fs');
const jwt = require('jsonwebtoken');



var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');



//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
//var publicKey = fs.readFileSync('./server/routes/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

router.get('/listaelecciones', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/geteleccion', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
router.post('/elecciones',   async (req, res, next) => {

    console.log("insert eleccion "+req.body.titulo);
    var jsonsalida : any = {406: "error con comunicación a base de datos"};
    try{
        let ideleccion = await db.insertarEleccion(req.body.titulo,req.body.descripcion,req.body.fecha_inicio,req.body.hora_inicio,req.body.fecha_fin,req.body.hora_fin, req.body.opciones);
        
        jsonsalida = {"estado":201,"id":ideleccion,"titulo":req.body.titulo,"descripcion":req.body.descripcion,"fecha_inicio":req.body.fecha_inicio,"hora_inicio":req.body.fecha_inicio,"fecha_fin":req.body.fecha_fin,"hora_fin":req.body.hora_inicio,"opciones":req.body.opciones};
        res.json(jsonsalida);
        addLog("eleccion insertardo:"+req.body.titulo+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }


    
});



function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];

        req.jwt=bearerToken;

        next();
    }else{
        res.sendStatus(403) //ruta o acceso prohibido
    }
}
module.exports = router