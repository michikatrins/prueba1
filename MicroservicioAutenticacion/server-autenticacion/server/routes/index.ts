var express = require('express');
var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var fs = require('fs');
/*
const path = require('path');
const jwt = require('jsonwebtoken');
*/

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');



//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
//var publicKey = fs.readFileSync('./server/routes/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

router.get('/listaregistro', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/getregistro', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/getpais', async (req, res, next) => {
    try{
        let results = await db.allPais();
        res.json(results);
        addLog("paises consultados"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

/*
// ruta index
router.get('/index', function (req, res) {
    console.log("Ruta index");
    res.json({
        mensaje: "Microservicio de Autenticacion"
    });
});

// endpoint para el login de usuarios
router.post('/login',function (req, res) {
    const user = req.body;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM usuario WHERE correo = ?", [user.correo], (err, rows) => { 
            if(err){
                console.log(err);
                res.json(err);
            }
            if(user.password == rows[0].password){
                addLog(" >> Login satisfactorio para usuario ");
                res.json({
                    mensaje: "Login satisfactorio.",
                    rows,
                });
            }else{
                res.json({
                    mensaje: "Login Error.",
                    code: 400,
                });
            }
        });
    });
});

// obtener usuario por id
router.get('/usuario/:id', function (req, res) {
    const  id  = req.params.id;
    console.log(id);
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM usuario WHERE id = ?", [id], (err, rows) => {
            if(err){
                console.log(err);
                res.json(err);
            }
            addLog(" >> Usuario existe.");
            res.json({
                mensaje: "Usuario existe.",
                rows: rows[0]
            });
        });
    });
});

//actualizar un usuario
router.put('/usuario/:id', function (req, res) {
    const id = req.params.id;
    const user = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE usuario set ? where id = ?', [user, id], (err, rows) => {
            addLog(" >> Usuario actualizado.");
            res.json({
                mensaje: "Usuario actualizado."
            });
        });
    });
});

//eliminar un usuario
router.delete('/usuario/:id', function (req, res) {
    const id = req.params.id;
    req.getConnection((err, conn) => {
        conn.query('DELETE from usuario where id = ?', [id], (err, rows) => {
            addLog(" >> Usuario eliminado.");
            res.json({
                mensaje: "Usuario eliminado."                       
            });
        });
    });                
});
*/

module.exports = router