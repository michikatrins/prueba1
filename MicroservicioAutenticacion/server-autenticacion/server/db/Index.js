var axios = require('axios');
const mysql = require('mysql');

var fs = require('fs');

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

const con = mysql.createConnection({

  
    host: 'db-mysql-autenticacion',
    port: '3306',
    user: 'admin',
    password: 'root',

    database: 'prsa'
});

let proyectobd = {};

proyectobd.all = () => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM renap;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });
};

proyectobd.one = (idrenap) => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM renap where id_renap =" + idrenap + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });


};


proyectobd.allPais = () => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM pais;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });
};

module.exports = proyectobd;