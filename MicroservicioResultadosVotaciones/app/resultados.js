
module.exports.Resultados= class Resultados{

    urlMicroservicioVotacion=''
    
    constructor() {
    }
    
    isIdValido(id){
        if(id<=0){
            return false;
        }else{
            return true;
        }
    }

    isValidJsonResult(data){
        if(data!=undefined){
            if(data.status==200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    jsonHasAnError(){
        if(data!=undefined){
            if(data.status!=200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    async llamarMicroserviciosVotacion(id,bearerToken,callback){

        axios
        .post(this.urlMicroservicioVotacion, {
            id_eleccion:id,llave:''
        },{
            headers:{
                'Authorization': `Bearer ${bearerToken}` 
            }
        })
        .then(res => {
            console.log(`statusCode: ${res.statusCode}`)
            console.log(res);
            return callback(null,res);
        })
        .catch(error => {
            console.error(error);
            return callback(error,null);
        })
    }

    sumar(num){
        return num+num;
    }
}

//module.exports=Resultados();