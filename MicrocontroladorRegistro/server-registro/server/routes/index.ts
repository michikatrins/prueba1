var express = require('express');
var db = require('../db/index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var fs = require('fs');
/*
const path = require('path');
const jwt = require('jsonwebtoken');
*/

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');



//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
//var publicKey = fs.readFileSync('./server/routes/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

router.get('/listaregistro', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/getregistro', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/getpais', async (req, res, next) => {
    try{
        let results = await db.allPais();
        res.json(results);
        addLog("paises consultados"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/getDepartamento',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let departamento = await db.allDepartamento(req.body.idpais);
        jsonsalida = {"estado":201,"iddepartamento": departamento.iddepartamento,"nombre": departamento.nombre,"idpais": departamento.idpais};
        res.json(departamento);
        addLog("departamento consultados"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/getMunicipio',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let municipio = await db.allMunicipio(req.body.iddepartamento);
        jsonsalida = {"estado":201,"idmunicipio": municipio.idmunicipio,"nombre": municipio.nombre,"iddepartamento": municipio.iddepartamento};
        res.json(municipio);
        addLog("municipios consultados"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.post('/registro',   async (req, res, next) => {
    var jsonsalida : any = {406: "error con comunicación a base de datos"};
    try{
        let idrenap = await db.insertarEleccion(req.body.Cui,req.body.Nombres,req.body.Apellidos,req.body.Fecha_nacimiento,req.body.Lugar_municipio,req.body.Nacionalidad, req.body.Sexo,req.body.Estado_civil,req.body.Servicio_militar,req.body.Privado_libertad,req.body.Padron,req.body.Foto,req.body.Email,req.body.Ip);
        jsonsalida = {"estado":201,"id":idrenap,"cui":req.body.Cui,"nombres":req.body.Nombres,"Apellidos":req.body.Apellidos,"Fecha_nacimiento":req.body.Fecha_nacimiento,"Lugar_municipio":req.body.Lugar_municipio,"Nacionalidad":req.body.Nacionalidad,"Sexo":req.body.Sexo};
        res.json(jsonsalida);
        addLog("eleccion insertardo:"+req.body.titulo+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


module.exports = router