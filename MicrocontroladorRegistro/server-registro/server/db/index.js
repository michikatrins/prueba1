var axios = require('axios');
const mysql = require('mysql');

var fs = require('fs');

var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})
function addLog(newlog){
    logger.write(newlog+"\n");
}

const con = mysql.createConnection({

  
    host: 'db-mysql-registro',
    port: '3306',
    user: 'admin',
    password: 'root',

    database: 'prsa'
});
let proyectobd = {};

proyectobd.all = () => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM renap;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });
};

proyectobd.one = (idrenap) => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM renap where id_renap =" + idrenap + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });


};


proyectobd.allPais = () => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM pais;";
        con.query(query, (err, res) => {
        if (err) throw err;           

        resolve(res);
        });
    });
};

proyectobd.allDepartamento = (idpais) => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM departamento where idpais =" + idpais + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });
};

proyectobd.allMunicipio = (iddepartamento) => {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM municipio where iddepartamento = " + iddepartamento + ";";
        con.query(query, (err, res) => {
        if (err) throw err;           
        return resolve(res);
        });
    });
};
/*
create table `Renap`(
    `id_renap` INT NOT NULL AUTO_INCREMENT,
 `Cui` int not null,
 `Nombres` varchar(100) not null,
 `Apellidos` varchar(100) not null,
 `Fecha_nacimiento` date,
 `Lugar_municipio` varchar(100) not null,
 `Lugar_departamento` varchar(100) not null, 
 `Lugar_pais` varchar(50) not null,
 `Nacionalidad` varchar(10) not null,
 `Sexo` varchar(2) not null,
 `Estado_civil` int not null,
 `Servicio_militar` int not null,
 `Privado_libertad` int not null,
 `Padron` int not null,
 `Foto` varchar(100) not null,
 `Email` varchar(100) not null,
 `IP` varchar(100) not null,
    PRIMARY KEY ( id_renap )
);
*/

proyectobd.insertarRenap = (Cui,Nombres,Apellidos,Fecha_nacimiento,Lugar_municipio,Lugar_departamento,
    Lugar_pais,Nacionalidad,Sexo,Estado_civil,Servicio_militar,Privado_libertad, Padron, Foto, Email, Ip) => {        
    return new Promise((resolve, reject) => {
        const query = "INSERT INTO Renap(Cui,Nombres,Apellidos,Fecha_nacimiento,Lugar_municipio,Lugar_departamento,Lugar_pais,Nacionalidad,Sexo,Estado_civil,Servicio_militar,Privado_libertad,Padron,Foto,Email,Ip) VALUES ("+Cui+",'" + Nombres + "','" + Apellidos + "','" + Fecha_nacimiento + "','"+ Lugar_municipio +"','" + Lugar_departamento + "','"+ Lugar_pais +"','"+ Nacionalidad+"','"+Sexo +"',"+Estado_civil+","+Servicio_militar+","+Privado_libertad+","+Padron+",'"+Foto+"','"+Email+"',"+Ip+");";
        con.query(query, (err, res) => {
        if (err) throw err;           
        addLog("registro creado: "+res.insertId+"\n");
        resolve(res.insertId);
        });
    });
};

module.exports = proyectobd;


/*
//update
//actualizar un usuario
app.put('/actualizarUsuario/:id', function (req, res) {
    const id = req.params.id;
    const user = req.body;
    console.log(user);
    req.getConnection((err, conn) => {
        conn.query('UPDATE Renap set nombres = ? where id_renap = ?', [user.nombres, id], (err, rows) => {
            //addLog(" >> usuario actualizado.");
            res.json({
                //header: decoded.header,
                //payload: decoded.payload,
                mensaje: "usuario actualizado.",
            });
        });
    });
});

//delete
/*
app.delete('/eliminarUsuario/:id', function (req, res) {
    const id = req.params.id;
    const user = req.body;
    console.log(user);
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM Renap where id_renap = ?', [id], (err, rows) => {
            //addLog(" >> usuario actualizado.");
            res.json({
                //header: decoded.header,
                //payload: decoded.payload,
                mensaje: "usuario eliminado.",
            });
        });
    });
});
*/
