const assert = require('assert');
const Registro=require('../server/registro');

let microservicio=new Registro.Registro();


describe('Test Microservicio registro', () => {
    it('Is valid Json response from votacion microservice {status:200} should return true', () => {
            const respuesta={status:200,titulo:'Elecciones Presidente'};
            const res=microservicio.isValidJsonResult(respuesta);
            assert.ok(res);
       });
    it('Validate negative id -1 should return false', () => {
            const res=microservicio.isIdValido(-1);
            assert.equal(res,false);
       });
    it('Validate positive id 4 should return true', ()=>{
            const res=microservicio.isIdValido(4);
            assert.ok(res);
        });
   });
